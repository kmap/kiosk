#! /usr/bin/env make -f

SHELL = /bin/sh
INSTALL = install
RM = rm
PYTHON = /usr/bin/python

D ?= build
DESTDIR ?=

prefix = /usr
sysconfdir = /etc
bindir = ${prefix}/bin
libdir = ${prefix}/lib
datarootdir = ${prefix}/share

.PHONY: default
default: help

.PHONY: help
help:
	@echo "$$HELP_MSG"

.PHONY: build
build:
	$(PYTHON) -m build --wheel --outdir=${D} --no-isolation

.PHONY: clean
clean:
	[[ "$(realpath ${D})" =~ "$(realpath .)" ]] # Safety measure: forbid removing directories outside of source tree
	-$(RM) -vrf ${D}
	-$(RM) -vrf src/*.egg-info

.PHONY: install
install: whl_file = $(lastword $(wildcard ${D}/*.whl))
install:
	$(INSTALL) -vDm 644 -t ${DESTDIR}${datarootdir}/factory/etc/kmap/kiosk/ \
		dist/app.env \
		dist/webapp.toml
	$(INSTALL) -vDm 644 -t ${DESTDIR}${datarootdir}/applications/ \
		dist/xdg/org.paradoxdev.kmap.desktop
	$(INSTALL) -vDm 644 -t ${DESTDIR}${sysconfdir}/nginx/sites-available/ \
		dist/nginx/kmap-kiosk.conf
	$(INSTALL) -vDm 644 -t ${DESTDIR}${libdir}/systemd/system/ \
		dist/systemd/kmap-kiosk-webapp.service
	$(INSTALL) -vDm 644 -t ${DESTDIR}${libdir}/systemd/user/ \
		dist/systemd/kmap-app-launcher.service
	$(INSTALL) -vDm 755 -t ${DESTDIR}${bindir}/ \
		src/kmap-app

	$(INSTALL) -vDm 644 -T dist/kmapkiosk.tmpfiles ${DESTDIR}${libdir}/tmpfiles.d/kmapkiosk.conf

	$(PYTHON) -m installer --destdir=${DESTDIR}/ --prefix=${prefix} ${whl_file}

.PHONY: test
test:
	$(PYTHON) -m pipenv run app

# DEFINITIONS:
define HELP_MSG :=
TARGETS:
  default _______________ run the default target (help)
  help __________________ show this help message
  build _________________ build the project's wheel distribution file
  clean _________________ remove intermediate build files
  install _______________ install the project
  test __________________ run the webapp in development mode

OPTIONS:
  D _____________________ build directory [default: "./build"]
  DESTDIR _______________ directory for staged installs
  prefix ________________ install prefix [default: "/usr"]
  configdir _____________ config file base directory [default: "/etc"]
  bindir ________________ executable program base directory [default: "$${prefix}/bin"]
  libdir ________________ read-only program data base directory [default: "$${prefix}/lib"]
  datarootdir ___________ shared resource base directory [default: "$${prefix}/share"]
endef
export HELP_MSG
