KMap Kiosk
==========

Getting Started
---------------

__Dependencies (ArchLinux):__

- `chromium`: browser for viewing web-app in kiosk mode

Development
-----------

__Dependencies (ArchLinux)__

- `pip`: package and distribute
- `python-pipenv`: virtual environment support
