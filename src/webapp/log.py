import flask
import json
from .utils import SystemError


class StructLog:
    @classmethod
    def from_error(cls, err: SystemError, *args, **kwargs) -> "StructLog":
        return cls(*args, internal={
            "code": err.code,
            "message": err.msg,
            "info": err.info,
        }, **kwargs)

    def __init__(self, msg: str = "", **kwargs):
        self.msg = msg
        self.kwargs = kwargs
        self.request = dict(
            host=flask.request.host,
            url=flask.request.base_url,
            args=flask.request.args,
            data=flask.request.get_data(as_text=True),
        )

    def __str__(self) -> str:
        return json.dumps(dict(
            message=self.msg,
            info=self.kwargs,
            request=self.request
        ))
