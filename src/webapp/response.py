import enum
from .utils import SystemError


@enum.unique
class StatusCode(enum.IntEnum):
    OK = 200
    BAD_REQUEST = 400
    INTERNAL_SERVER_ERROR = 500
    NOT_IMPLEMENTED = 501

    def __str__(self) -> str:
        return self.name


class Error(dict[str, str]):
    def __init__(self, ecode: StatusCode, msg: str = "", **kwargs):
        self.ecode = ecode
        self.msg = msg
        self.info = kwargs
        super().__init__({"type": str(ecode), "message": msg, "info": kwargs})


class Response:
    def __init__(self,
                 code: int,
                 data: dict = {},
                 *,
                 errors: list[dict[str, str]] = []):
        self.code = code
        self.data = data
        self.errors = errors

    def encode(self) -> tuple[dict, int]:
        return {"errors": self.errors, "data": self.data}, self.code


class Ok(Response):
    def __init__(self, data: dict = {}):
        super().__init__(int(StatusCode.OK), data)


class ResponseError(Response):
    def __init__(self,
                 etype: StatusCode,
                 msg: str,
                 *,
                 data: dict = {}, **kwargs):
        super().__init__(int(etype), data, errors=[
            Error(etype, msg, **kwargs),
        ])


class BadRequest(ResponseError):
    def __init__(self, msg: str = "Bad Request", **kwargs):
        super().__init__(StatusCode.BAD_REQUEST, msg, **kwargs)


class InternalServerError(ResponseError):
    @classmethod
    def from_error(cls,
                   err: SystemError,
                   *args,
                   **kwargs) -> "InternalServerError":
        return cls(*args, **kwargs, internal={
            "code": err.code,
            "message": err.msg,
            "info": err.info,
        })

    def __init__(self, msg: str = "Internal Server Error", **kwargs):
        super().__init__(StatusCode.INTERNAL_SERVER_ERROR, msg, **kwargs)


class NotImplemented(ResponseError):
    def __init__(self, msg: str = "Not Implemented", **kwargs):
        super().__init__(StatusCode.NOT_IMPLEMENTED, msg, **kwargs)
