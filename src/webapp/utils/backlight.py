import os
from . import SystemResult, SystemError, SystemOk, config


def _get_sysfsdev() -> str | None:
    sysfsdev = config.get_global()["machine"]["sysfs"]["backlight"]["device"]
    return sysfsdev if sysfsdev else None


def _file2int(path: str) -> int:
    with open(os.path.join(path), "rb") as file:
        data = file.read()
        return int(data)


def get_brightness_max(sysfs_path: str | None = None) -> SystemResult[int]:
    sysfsdev = sysfs_path or _get_sysfsdev()
    if sysfsdev is None:
        return SystemError("backlight SYSFS device not configured")

    try:
        val = _file2int(os.path.join(sysfsdev, "max_brightness"))
    except OSError as e:
        return SystemError("failed to read backlight max brightness",
                           e.errno, exception=repr(e))
    except ValueError as e:
        return SystemError("failed to parse backlight max brightness",
                           exception=repr(e))
    else:
        return SystemOk(val)


def _get_brightness_max_nonzero(*args, **kwargs) -> SystemResult[int]:
    result = get_brightness_max(*args, **kwargs)
    if not result:
        return result

    maxval = result.get()
    if maxval == 0:
        return SystemError("max backlight brightness is invalid (0)")
    else:
        return SystemOk(maxval)


def get_brightness() -> SystemResult[float]:
    sysfsdev = _get_sysfsdev()
    if sysfsdev is None:
        return SystemError("backlight SYSFS device not configured")

    result = _get_brightness_max_nonzero(sysfsdev)
    if not result:
        return result
    maxval = result.get()

    try:
        val = _file2int(os.path.join(sysfsdev, "brightness"))
    except OSError as e:
        return SystemError("failed to read backlight brightness",
                           e.errno, exception=repr(e))
    except ValueError as e:
        return SystemError("failed to parse backlight brightness",
                           exception=repr(e))
    else:
        return SystemOk(val / maxval)


def set_brightness(level: float = 1.0) -> SystemResult[int]:
    sysfsdev = _get_sysfsdev()
    if sysfsdev is None:
        return SystemError("backlight SYSFS device not configured")

    result = _get_brightness_max_nonzero(sysfsdev)
    if not result:
        return result
    maxval = result.get()

    val = int(level * maxval)
    try:
        with open(os.path.join(sysfsdev, "brightness"), "wb") as file:
            file.write(str(val).encode(encoding='ascii'))
    except OSError as e:
        return SystemError("failed to write backlight brightness",
                           e.errno, exception=repr(e))
    else:
        return SystemOk(val)
