import tomli
import os
from collections import defaultdict
from . import SystemResult, SystemError, SystemOk

DEFAULT_PATH = os.environ.get(
    "KMAP_WEBAPP_CONFIG",
    default="/etc/kmap/kiosk/webapp.toml",
)


class ConfigDict(defaultdict):
    def __init__(self, *args, **kwargs):
        super().__init__(ConfigDict, *args, **kwargs)

    def __setitem__(self, key, value, /):
        if isinstance(value, dict):
            defaultdict.__setitem__(self, key, ConfigDict(value))
        else:
            defaultdict.__setitem__(self, key, value)


class Config(ConfigDict):
    @classmethod
    def load(cls, path: str):
        cfg = cls()
        try:
            with open(path, "rb") as file:
                tomlcfg = tomli.load(file)
                cfg.update(tomlcfg)
        except OSError as e:
            raise OSError("failed to open config file for reading") from e
        finally:
            return cfg


_SINGLETON: Config | None = None


def load_global(path: str = DEFAULT_PATH) -> SystemResult[Config]:
    err = None
    try:
        cfg = Config.load(path)
    except OSError as e:
        cfg = Config()
        err = SystemError(str(e))
    finally:
        global _SINGLETON
        _SINGLETON = cfg
        return SystemOk(cfg) if err is None else err


def get_global(*, lazy: bool = True) -> Config:
    if _SINGLETON is None:
        if lazy:
            load_global()

        if _SINGLETON is None:
            raise RuntimeError("config singleton is not loaded")
    return _SINGLETON
