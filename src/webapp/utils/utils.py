import json
from typing import TypeVar, Generic

T = TypeVar("T")


class SystemResult(Generic[T]):
    def __init__(self, ok: bool, result: T | None = None):
        self.ok = ok  # Required because T may be None
        self.result = result

    def __str__(self) -> str:
        raise NotImplementedError()

    def __bool__(self) -> bool:
        return self.ok


class SystemOk(Generic[T], SystemResult[T]):
    def __init__(self, result: T):
        super().__init__(True, result)

    def __str__(self) -> str:
        return str(self.result)

    def get(self) -> T:
        return self.result


class SystemError(Generic[T], SystemResult[T]):
    def __init__(self, msg: str = "", code: int = -1,  **kwargs):
        self.msg = msg
        self.code = code
        self.info = kwargs
        super().__init__(False)

    def __str__(self) -> str:
        return " ".join(
            filter(None, [self.msg,
                          f"[{self.code}]",
                          json.dumps(self.info) if self.info else None,
                          ]))
