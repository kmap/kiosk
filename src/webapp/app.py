import flask
from flask import Flask
from . import response
from .utils import config, backlight
from .log import StructLog as Log
from random import randbytes

app = Flask(__name__)
app.secret_key = randbytes(64)


def define_session_backlight(level: float | None = None) -> bool:
    if level is None:
        session_val = flask.session.get("backlight")
        if session_val is not None:
            return True  # already defined
        elif session_val is None:
            result = backlight.get_brightness()
            if not result:
                return False
            level = result.get()

    flask.session["backlight"] = level if level > 0.0 else 0.5
    return True


@app.route("/")
def index():
    app.logger.debug(Log())
    return flask.send_file('static/index.html')


@app.route("/home")
def home():
    res = config.get_global()["resources"]
    context = {
        "qwc2": {
            "url": res["qwc2"].get("url", "//localhost:8080"),
        },
    }
    app.logger.debug(Log(context=context))
    return flask.render_template('home.html', **context)


@app.route("/settings")
def settings():
    app.logger.debug(Log())
    has_backlight = define_session_backlight()
    if not has_backlight:
        app.logger.warning(Log("backlight could not be found"))
    return flask.render_template('settings.html')


@app.route("/app.css")
def app_css():
    return flask.redirect(flask.url_for('static', filename='app.css'))


@app.post("/api/host/backlight")
def api_host_backlight():
    level_str = flask.request.args.get("level")
    if level_str is None:
        app.logger.info(Log("request error: 'level' not provided"))
        return response.BadRequest("expected 'level' argument").encode()

    try:
        level = float(level_str)
    except Exception:
        app.logger.info(
            Log(f"request error: level='{level_str}' is not a float"))
        return response.BadRequest("expected float for 'level'").encode()
    else:
        if level < 0.0 or level > 1.0:
            app.logger.info(
                Log(f"request error: level={level} is not a percent"))
            return response.BadRequest("expected percent for 'level'").encode()
        else:
            app.logger.info(Log("setting backlight brightness to " +
                            "{percent}%".format(percent=level * 100.0)))

        result = backlight.set_brightness(level)
        if result:
            define_session_backlight()
            return response.Ok().encode()
        else:
            app.logger.error(Log.from_error(result, "failed to set backlight"))
            return response.InternalServerError.from_error(
                result,
                "failed to change backlight",
            ).encode()


@app.post("/api/event/touch")
def api_event_touch():
    result = backlight.get_brightness()
    if not result:
        app.logger.error(Log.from_error(result, "failed to get backlight"))
        return response.InternalServerError.from_error(
            result,
            "failed to get backlight",
        ).encode()

    level = result.get()
    if level == 0.0:
        old_level = flask.session["backlight"]
        app.logger.info(
            Log("restoring backlight to {percent}% after touch event".format(
                percent=old_level * 100.0)))

        result = backlight.set_brightness(old_level)
        if result:
            return response.Ok(data={"done": "true"}).encode()
        else:
            app.logger.error(Log.from_error(result, "failed to set backlight"))
            return response.InternalServerError.from_error(
                result,
                "failed to change backlight",
            ).encode()
    else:
        app.logger.debug(
            Log("touch event ignored; backlight at {percent}%".format(
                percent=level * 100.0)))
        return response.Ok(data={"done": "false"}).encode()


if __name__ == "__main__":
    app.run(debug=True)
